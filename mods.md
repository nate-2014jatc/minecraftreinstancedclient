| Mod Name                      |     Version     |      Approved?     | Server Side?             | Client Side?             |
|-------------------------------|:---------------:|:------------------:|--------------------------|--------------------------|
| Additional Banners            |      1.1.76     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Antique Atlas                 |      4.5.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Artisan's Tabs                |     1.3.0.3     | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Atlas Extras                  |       1.6       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| AutoRegLib                    |      1.3-26     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Baubles                       |      1.5.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Bauble of undying             |      1.0.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Baubles shulker boxes         |    1.12.2-4r    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Better Foliage                |      2.2.0      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Better FPS                    |      1.4.8      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Bibliocraft                   |      2.4.5      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Biomes O'Plenty               |    7.0.1.2419   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Block Craftery                |      1.2.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Bloodmoon                     |      1.5.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Buildcraft                    |    7.99.24.1    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Can I join Now?               |      1.1.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_multiplication_x: |
| Chameleon                     |      4.1.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Chameleon Creepers            |      1.4.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Chisel                        | 0.2.1.35        | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Chisels and Bits              |      14.31      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Chunk Animator                |       1.2       | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Cloche Call                   |      1.1.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Clumps                        |      3.1.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| CodeChickenLib                |    3.2.2.353    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Colored Lights                |      1.1.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Colytra                       |      1.1.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Controlling                   |      3.0.6      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| CoroUtil Library              |      1.2.31     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Corpse Complex                |     1.0.5.1     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Craft Tweaker 2               |      4.1.17     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Crates Felt Blu               |       1.0       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Creative Core                 |      1.9.43     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| CTM                           |    0.3..3.22    | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Custom LAN Ports              |        1b       | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Custom NPCs                   |     30jan19     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Discord Suite                 |      2.2.4      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Doggy Talents                 |    1.14.2.438   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Dynamic Surroundings          |     3.5.4.3     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Dynamic Sword Skills          |      6.0.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Dynmap                        |      3.0-b3     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_multiplication_x: |
| Dynmap Block Scan             |      3.0-b1     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_multiplication_x: |
| Engineer's Doors              |      0.8.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Engineered Golems             |       1.2a      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Expanded Arcanum              |      1.0.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Extra Bit Manipulation        |      3.4.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Extra Golems                  |      7.1.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Extreme Reactors              |     0.4.5.66    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Faucets and Filters           |      1.0.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Foamflower                    |    1.0.0.0-b1   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Forgelin                      |      1.8.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Forgiving Void                |      1.0.21     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Friendly Fire                 |      1.5.10     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| FTBLib                        |     5.4.1.96    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| FTB Utilities                 |     5.4.0.94    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Golrith's Texturepack         |     Unknown     |   :grey_question:  | :grey_question:          | :grey_question:          |
| Guide-API                     |     2.1.8-63    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| HammerCore                    |     2.0.4.4     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Hex Lands                     |      1.9.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| HWYLA                         |    1.8.26-b41   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Immersive Engineering         |     0.12-89     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Immersive Posts               |      0.1.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Immersive Railroading         |  1.5.0_1.12-402 | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Immersive Tech                |      1.3.10     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Immersive Vehicles            |      13.1.0     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| I.V. Seagull's Civ            |      1.14.0     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| I.V. Seagull's Mil            |      1.6.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Infused Ring                  |      1.0.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| InGame Config Manager         |      1.2.8      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| IVToolkit                     |      1.3.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Jukebox Mod                   |       1.2       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Just Enough Items             |    4.15.0.268   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Just Enough Reactors          |     1.1.3.61    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Knowledge Share               |       1.0       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Lag Goggles                   |       4.1       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Login Shield                  |    3-g7c6e7ac   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Minecraft Transport Simulator |     Unknown     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Modern Lights                 |      1.0.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Modpack Config Checker        |       1.7       | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| More Fuels Mod                |      1.6.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| MTS Official Vehicle Pack     |       V09       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| MTS Caterpillar  Pack         |      1.1.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| MultiStorage                  |      1.4.13     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Mysql Connector Java          |      5.1.47     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_multiplication_x: |
| MysticalLib                   |      1.1.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Nether Portal Fix             |      5.3.17     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| NoCtrl                        |       1.2       | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Open Blocks                   |      1.8.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Open Computers                |    1.7.4.153    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Open Mods                     |      0.12.2     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Open Printer                  |     0.1.0.2     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Open Security                 |      1.0-23     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| OreLib                        |     3.5.2.2     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Pick Block Plus               |      1.1.1      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Ping                          |      1.4.5      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Placebo                       |      1.6.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| PlayerRevive                  |      1.2.25     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Portality                     |     1.2.2-14    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Quark                         |     r1.5-146    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Reccurent Complex             |     1.4.8.2     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Redstone Flux                 |     2.1.0.6     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Redstone Guages and Switches  |      1.1.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Reforged                      |      0.7.5      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Rustic                        |      1.1.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Rustic BOP Woods              |       1.0       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Rustic Thaumaturgy            |     Unknown     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Scaling Health                |    1.3.34+135   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Server Tick Monitor           |      3.0.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| shetiphiancore                |      3.5.9      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| shulkerbox                    |      1.3.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Silent Lib                    |    3.0.13+167   | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Simple Dimensions             |      1.3.1      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| SimpleZoom                    |      1.3.2      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Skylands                      |      1.5.5      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Smooth Font                   |      1.16.2     | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Splash Animations             |      0.2.1      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Storage Drawers               |      5.3.8      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Storage Drawers Extras        |     Unknown     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| SwordSkillsAPI                |      1.1.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thaumcraft                    |     6.1-b26     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thaumcraft Inventory Scanning |      2.0.10     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thaumic Augmentation          |      1.0.3      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thaumic Computers             |      0.4.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thaumic JEI                   |     1.5.8-26    | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Thaumic Periphery             |      0.3.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| The Lost Cities               |      2.0.17     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| The Weirding Gadget           |      2.0.13     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thermal Dynamics              |     2.5.4.18    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Thermal Foundation            |     2.6.2.26    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| The SpotLight Mod             |    1.2.0.101    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Title Changer                 |      1.1.3      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| ToastControl                  |      1.8.0      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| Torch Bandolier               |     0.1.1-4     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Totem Expansion               |      1.2.2      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Track API                     |       1.1       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Treecapitator Port            |       0.9a      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| u_team_core                   |     2.2.4.94    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Unlimited Chisel Works        |      0.2.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| UNU Parts Pack                |      1.10.0     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| VanillaFix                    |    1.0.10-99    | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Various Oddites               |       4.0       | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Vending Block                 |     3.0.1.2     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Villager Trade Tables         |      0.6.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Waila                         |     Unknown     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Wars FTB Dynmap               |      1.0.4      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_multiplication_x: |
| Waystones                     |      4.0.67     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Weather2                      |      2.6.12     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| What Are We Looking At        |     2.5.269     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| World Edit                    | 6.1.10-SNAPSHOT | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| World Edit CUI                |      2.1.2      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| World Handler                 |      2.2.2      | :heavy_check_mark: | :heavy_multiplication_x: | :heavy_check_mark:       |
| World Primer                  |      0.5.0      | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |
| Zero Core                     |     0.1.2.8     | :heavy_check_mark: | :heavy_check_mark:       | :heavy_check_mark:       |